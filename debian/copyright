Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Config-Grammar
Upstream-Contact: David Schweikert <david@schweikert.ch>
Source: https://metacpan.org/release/Config-Grammar

Files: *
Copyright: Copyright (C) 2000-2005 by ETH Zurich. All rights reserved.
           Copyright (C) 2007-2016 David Schweikert. All rights reserved.
License: GPL-1+ or Artistic

Files: debian/*
Copyright: 2009-2022, Salvatore Bonaccorso <carnil@debian.org>
License: GPL-1+ or Artistic

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
